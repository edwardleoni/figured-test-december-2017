<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth Routes
Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');

// Admin Routes
Route::get('/admin', ['as' => 'admin', 'uses' => 'Admin\PostController@showIndex']);
Route::get('/admin/add', ['as' => 'admin.add', 'uses' => 'Admin\PostController@showAdd']);
Route::post('/admin/add', ['as' => 'admin.add', 'uses' => 'Admin\PostController@add']);
Route::get('/admin/{postId}/edit', ['as' => 'admin.edit', 'uses' => 'Admin\PostController@showEdit']);
Route::post('/admin/{postId}/edit', ['as' => 'admin.edit', 'uses' => 'Admin\PostController@edit']);
Route::get('/admin/{postId}/remove', ['as' => 'admin.remove', 'uses' => 'Admin\PostController@remove']);

// Public routes
Route::get('/', ['as' => '', 'uses' => 'BlogController@index']);
Route::get('/{postId}', ['as' => 'post', 'uses' => 'BlogController@showPost']);
