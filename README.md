# The task
```
I would like you to build a very simple blog application where users can read posts and where an admin user can login and create, update and delete posts.
If you could use the following as part of the project:
Laravel 5.5
PHP7
MongoDB for storing the blog posts - OK to use MySQL for users as this comes standard in Laravel.
Can use whatever you like for the front end, plain Laravel blade is fine.
```

# Installation

## Permissions
Depending on your OS and on how Docker is configured, you may need to set some permissions. Just to be safe, run these:
`sudo chmod -R 777 bootstrap/cache/` and `sudo chmod -R 777 storage/`

Not doing so could cause misleading errors such as "The page has expired due to inactivity.". Damn you Taylor Otwell!


## This is a Docker friendly project!
To make things faster for you, and since this is meant to run locally only and just as a test, I have commited the .env file. Don't judge me, I'm just trying to help you here! ;)

Simply run:

`docker-compose up --force-recreate --build`

The above should take a while, go grab a coffee or talk about the weather with a colleague in the mean time! When that's done run:

`bash docker/install_dependencies.sh`

Now sometimes composer can be really slow, if you feel asleep, you can try chitchating again, here are some ideas: `Hey, do you think the new Symfony could end Laravel's supremacy?`, `Hey did you watch the game of <insert sport here> last night?`. Once that's complete - to wrap it up - run:

`bash docker/migrate.sh`

Go to your browser and type:

`http://localhost:8910`

## Living on the edge
If you like living on the edge and has not joined the Docker club yet, you will need to meet the below requirements somehow:
- Composer
- MySQL/MariaDB Server
- MongoDB Server
- Apache/NginX
- PHP7

Start by copying the .env.example and name it after .env, fill up important data such as database details
Run `php key:generate`
Run `composer install`
Run `php artisan migrate`
