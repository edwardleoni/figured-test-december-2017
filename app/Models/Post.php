<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Jenssegers;

class Post extends Jenssegers
{
    protected $connection = 'mongodb';

    protected $fillable =  ['title', 'content', 'user_id'];
}
