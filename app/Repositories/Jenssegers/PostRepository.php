<?php

namespace App\Repositories\Jenssegers;

use App\Models\Post;
use App\Repositories\PostRepository as PostRepositoryInterface;
use Illuminate\Support\Facades\Auth;

/**
 * Only the author of the post is able to perform write actions
 * Read actions have a $validateAuthor against them that can be used as needed
 */
class PostRepository implements PostRepositoryInterface
{
    /**
     * Get all posts and order them by id
     *
     * @param bool $validateAuthor also queries against the author
     *
     * @return array
     */
    public function getAll($validateAuthor = false)
    {
        $post = new Post;

        if ($validateAuthor === true) {
            $post = $post->where('user_id', '=', Auth::id());
        }

        $post = $post->get()->sortByDesc('_id');

        return $post;
    }

    /**
     * Get a posts by its id
     *
     * @param integer $id
     * @param bool $validateAuthor also queries against the author
     *
     * @return array
     */
    public function getById($id, $validateAuthor = false)
    {
        $post = new Post;

        if ($validateAuthor === true) {
            $post = $post->where('user_id', Auth::id());
        }

        $post = $post->where('_id', $id)->first();

        return $post;
    }

    /**
     * Save a post and return its id
     *
     * @param array $data
     *
     * @return integer
     */
    public function add($data)
    {
        return Post::create([
            'title' => $data['title'],
            'content' => $data['content'],
            'user_id' => Auth::id()
        ]);
    }

    /**
     * Edit a post and return its id
     *
     * @param integer
     * @param array $data
     *
     * @return integer
     */
    public function edit($id, $data)
    {
        return Post::where([
            ['_id', '=', $id],
            ['user_id', '=', Auth::id()]
        ])->update([
            'title' => $data['title'],
            'content' => $data['content']
        ]);
    }

    /**
     * Remove a post
     *
     * @param integer $id
     *
     */
    public function remove($id)
    {
        Post::where([
            ['_id', '=', $id],
            ['user_id', '=', Auth::id()]
        ])->delete($id);
    }
}
