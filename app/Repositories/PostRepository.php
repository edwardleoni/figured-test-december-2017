<?php

namespace App\Repositories;

interface PostRepository
{
    public function getAll($validateAuthor);

    public function getById($id, $validateAuthor);

    public function add($data);

    public function edit($id, $data);

    public function remove($id);
}
