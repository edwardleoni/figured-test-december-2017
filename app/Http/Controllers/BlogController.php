<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Repositories\PostRepository;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PostRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $posts = $this->repository->getAll();

        return view('index', compact('posts'));
    }

    public function showPost(Request $request, $id)
    {
        $post = $this->repository->getById($id);

        return view('post', compact('post'));
    }
}
