<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\PostRepository;
use Illuminate\Http\Request;
use Redirect;
use Validator;

class PostController extends Controller
{
    /**
     * @var PostRepository || null $repository
     */
    private $repository = null;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PostRepository $repository)
    {
        $this->middleware('auth');
        $this->repository = $repository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function showIndex()
    {
        $posts = $this->repository->getAll(true);

        return view('admin/index', compact('posts'));
    }

    public function showAdd()
    {
        return view('admin/add');
    }

    public function add(Request $request)
    {
        $data = $request->all();

        $validator = self::validation([
            "title" => $data['title'],
            "content" => $data['content']
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator->errors())->withInput();
        }

        $this->repository->add($data);

        return Redirect(route("admin"));
    }

    public function showEdit(Request $request, $id)
    {
        $post = $this->repository->getById($id, true);

        return view('admin/edit', [
            "post" => $post
        ]);
    }

    public function edit(Request $request, $id)
    {
        $data = $request->all();

        $validator = self::validation([
            "title" => $data['title'],
            "content" => $data['content']
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator->errors())->withInput();
        }

        $post = $this->repository->edit($id, $data);

        return Redirect(route("admin"));
    }

    public function remove(Request $request, $id)
    {
        $this->repository->remove($id);

        return Redirect(route("admin"));
    }

    private function validation($data)
    {
        return Validator::make($data, [
            'title' => 'required|max:255',
            'content' => 'required|min:5',
        ]);
    }
}
